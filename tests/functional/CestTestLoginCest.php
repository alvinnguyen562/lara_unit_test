<?php
class CestTestLoginCest
{

    protected $myHelper;

    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function _inject(\Helper\MyHelper $myHelper) {
        $this->myHelper = $myHelper;
    }

    // tests
    public function tryToTest(FunctionalTester $I)
    {
        $I->wantTo('Test page login with dependency injection.');
        $this->myHelper->login('tuannguyen@mulodo.com', 'minhtuan');
    }
}
