<?php


class TuanTestCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->wantTo('Test page login.');
        $I->amOnPage('/');
        $I->see('Laravel');
        $I->click('Login');
        $I->see('Login');
        $I->fillField('email', 'tuannguyen@mulodo.com');
        $I->fillField('password', 'minhtuan');
        $I->click('#btn-login');
    }
}
