<?php

namespace Feature;

use Tests\TestCase;

class PostTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testCreatePost()
    // {
    //     $respone = $this->call('POST', 'posts');
    //     $respone->assertStatus(200);

    //     Post::create(["title" => "nguyen minh tuan", "description" => "ahihi ahihi ahihi"]);
    //     $this->assertDatabaseHas('post', ["title" => "nguyen minh tuan", "description" => "ahihi ahihi ahihi"]);
    // }

    public function testGetTextIpsumSuccess()
    {
        $mock = \Mockery::mock(PostController::class);
        $mock->shouldReceive('getTextIpsum')->once()->andReturn("success");
        $this->assertEquals('success', $mock->getTextIpsum());
    }

    public function testPrintText()
    {
        $mock = \Mockery::mock(BarTestService::class);
        $mock->shouldReceive('printText')->once()->andReturn("tuan");

        $this->assertEquals('tuan', $mock->printText());
    }

}
