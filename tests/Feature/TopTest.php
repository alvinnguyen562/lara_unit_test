<?php

namespace Tests\Feature;

use Tests\TestCase;

class TopTest extends TestCase
{
    // public function __construct()
    // {
    //     // We have no interest in testing Eloquent
    //     $this->mock = \Mockery::mock('Illuminate\Database\Eloquent\Model', Cemetery::class);

    // }

    public function tearDown()
    {
        \Mockery::close();
    }

    public function testIndex()
    {
        $mock = \Mockery::mock('Eloquent', Cemetery::class);
        $mock->shouldReceive('all')->once();
        $this->app->instance(Cemetery::class, $mock);

        $response = $this->call('GET', '/sp');

        $response->assertViewHas('listCemeteries');
    }

    public function testStatusSpTop()
    {
        $response = $this->get('/sp');

        $response->assertStatus(200);
    }

    public function testExistData()
    {
        $response = $this->get('/sp');

        $this->assertDatabaseHas('cemeteries', ['CEMETERY_ID' => 99]);
    }

}
