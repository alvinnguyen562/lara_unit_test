<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class MyHelper extends \Codeception\Module
{
    public function login($email = "", $password = "")
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/');
        $I->see('Laravel');
        $I->click('Login');
        $I->see('Login');
        $I->fillField('email', $email);
        $I->fillField('password', $password);
        $I->click('#btn-login');
    }
}
