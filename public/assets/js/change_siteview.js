function chnge_sitetype_pc(){
		
		var data = new Object();
	
		data.target_sitetype = "pc";

		$.ajax({
			url:'/common_lib/change_siteview.php',
			type:'POST',
			data:data,
			dataType:'text',
			async: false,
			success:function(res_json) {

				results = res_json.split(",");
				
				//window.alert(decodeURIComponent(results[1]));
				
				location.href = '/';
				
				return true;
			
			},
				error:function(res, status, e) {
			}
		});
				
		return false;

}

function chnge_sitetype_sp(){

		var data = new Object();
	
		data.target_sitetype = "sp";

		$.ajax({
			url:'/common_lib/change_siteview.php',
			type:'POST',
			data:data,
			dataType:'text',
			async: false,
			success:function(res_json) {

				results = res_json.split(",");
				
				location.href = '/sp/';
			
			},
				error:function(res, status, e) {
			}
		});
				
		return false;

}
