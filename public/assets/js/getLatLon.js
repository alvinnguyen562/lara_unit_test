var mobile = "";
var agent = navigator.userAgent;
if( agent.search(/iPhone/) != -1 ){
  mobile = "iPhone";
}else if( agent.search(/Android/) != -1 ){
  mobile = "Android";
}
//緯度経度取得
var gps;
var sitePath = "./";
function getGPS(updatePosition, path){

  if( path != null ){
    sitePath = path;	  
  }

  var gps = null;
  //if( mobile == "iPhone" ){
    gps = navigator.geolocation;
  //}else if( mobile == "Android" ){
  //  gps = google.gears.factory.create('beta.geolocation');
  //}
  if( gps != null ){
    gps.getCurrentPosition(searchUpdatePosition, handleError, {timeout:60000});
  }else{
	handleError();
  }
}

function searchUpdatePosition(position){
  var lat = position.coords.latitude;
  var lon = position.coords.longitude;

	// Ajax通信
	/*		
	var state_eng = "";
	var city_code = "";
	var data = new Object();

	data.lat = lat;
	data.lon = lon;
	
	$.ajax({
		url:'/common_lib/LGC_GetAddress.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			state_eng = decodeURIComponent(results[1]);
			city_code = decodeURIComponent(results[3]);

		},
		error:function(res, status, e) {
		}
	});
  
  location.href = sitePath + "/area/"+state_eng+"-"+city_code+".html" ;
  */
  location.href = sitePath + "/near/?lat=" + lat + "&lon=" + lon;
}

function handleError(positionError) {
  //alert('現在地の取得ができませんでした。電波状況の良い場所で再度お試しください。');
  //alert('現在地の取得ができませんでした。再度お試しください。');

  switch(positionError.code) {
    case positionError.UNKNOWN_ERROR:
        alert('現在地の取得ができませんでした。再度お試しください。'); //予期しないエラー
        break;
    case positionError.PERMISSION_DENIED:
        alert('ブラウザの設定で位置情報の利用が許可されていません。'); //位置情報の利用を許可しなかった場合
        break;
    case positionError.POSITION_UNAVAILABLE:
        alert('現在地の取得ができませんでした。ERROR CODE: ' + positionError.code); //プロバイダが内部エラー
        break;
    case positionError.TIMEOUT:
        alert('現在地の取得ができませんでした。再度お試しください。'); //位置情報取得処理がタイムアウト
        break;
  }
}
