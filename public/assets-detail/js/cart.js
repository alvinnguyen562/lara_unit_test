$(document).ready(function(){
		//スマホ用資料請求数
		if(0 < $("#cart_cnt").size()){
			if(Number($("#cart_cnt").val()) > 0){
				//window.alert(Number($("#cart_cnt").val()));
				/*$("#regist_area").css("visibility","visible");*/
				//$("div.request").hide();
				$.fixedUI('#regist_area');
			}else{
				//$("div.request").show();
				$.unfixedUI('#regist_area');
			}
		}
});

function click_cart_btn(id) {
	updateCart(id,"1");
	location.href = '/request/';
}

function click_cart_btn_sp(id) {
	updateCart(id,"1");
	location.href = '/sp/request/';
}

//-------------------------------------------------------------
//  PC資料請求リストClose
//-------------------------------------------------------------
function regist_cemetery_list_close() {
	$(".examination").attr("src","/common_img/header/btn_shiryou.png");
	$(".examination").removeClass("open")
	$("#examination-list").slideToggle("fast");
}

//-------------------------------------------------------------
//  PC資料請求リスト まとめて問い合わせのチェックボックス操作
//-------------------------------------------------------------
function regist_cemetery_all_check_ctrl() {
	if($("[name='regist_cemetery_check[]']:checked").length==$("[name='regist_cemetery_check[]']").length){
		$('#regist_cemetery_all_check').prop("checked",true);
	}else{
		$('#regist_cemetery_all_check').prop("checked",false);
	}
}

//-------------------------------------------------------------
//  PC資料請求リスト まとめてチェック
//-------------------------------------------------------------
function registCemeteryAllCheck(all_check_item) {
	if($('#regist_cemetery_all_check').prop("checked")){
		$("[name='regist_cemetery_check[]']").prop("checked",true);
	}else{
		$("[name='regist_cemetery_check[]']").prop("checked",false);
	}
	
	var sv_cart_list = "";
	$("[name='regist_cemetery_check[]']").each(function(index){
		if(sv_cart_list==""){
			sv_cart_list = "|"+$(this).val()+"|";
		}else{
			sv_cart_list += $(this).val()+"|";
		}
	});
	
	var data = new Object();
	data.cart_list = sv_cart_list;
	if($(all_check_item).prop('checked')==true){
		data.cstatus = "1";
	}else{
		data.cstatus = "2";
	}
	
	$.ajax({
		url:'/common_lib/LGC_CollectCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[0]));

		},
		error:function(res, status, e) {
		}
	});
	
	if($("[name='regist_cemetery_check[]']:checked").length>0){
		if($("#regist_cemetery_list_cnt").size()>0){
			$("#regist_cemetery_list_cnt").html($("[name='regist_cemetery_check[]']:checked").length);
		}

		if($("#regist_cemetery_list_cnt2").size()>0){
			$("#regist_cemetery_list_cnt2").html($("[name='regist_cemetery_check[]']:checked").length);
		}
		
		//一覧のチェックボックスチェック
		$("[name='regist_cemetery_check[]']:checked").each(function(index){
			if(0 < $("#cart_check_area"+$(this).val()).size()){
				//存在する
				if($("#cart_check_area"+$(this).val()).children('input').prop('checked')==false){
					$("#cart_check_area"+$(this).val()).css('background','#FF9999');
					$("#cart_check_area"+$(this).val()).children('input').prop({'checked':true});
					$("#cart_check_area"+$(this).val()).children('img').attr("src","/search/img/img_checkbox2_on.png");
				}
			}else{
				//存在しない
			}
		});
	}else{
		if($("#regist_cemetery_list_cnt").size()>0){
			$("#regist_cemetery_list_cnt").html("0");
		}

		if($("#regist_cemetery_list_cnt2").size()>0){
			$("#regist_cemetery_list_cnt2").html("0");
		}
		
		//一覧のチェックボックス解除
		$("[name='regist_cemetery_check[]']").each(function(index){
			if(0 < $("#cart_check_area"+$(this).val()).size()){
				//存在する
				$("#cart_check_area"+$(this).val()).css('background','#EAEAEA');
				$("#cart_check_area"+$(this).val()).children('input').prop({'checked':false});
				$("#cart_check_area"+$(this).val()).children('img').attr("src","/search/img/img_checkbox2_off.png");
			}else{
				//存在しない
			}
		});
	
	}
		
}

//-------------------------------------------------------------
//  PC資料請求リストhtml生成
//-------------------------------------------------------------
function regist_cemetery_list_html_pc(){

	var data = new Object();

	$.ajax({
		url:'/common_lib/registCemeteryListPC.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			if(0 < $("#examination-list").size()){
				//存在する
				$("#examination-list").html(decodeURIComponent(results[0]));
					
			}else{
				//存在しない
			}

		},
			error:function(res, status, e) {
		}
	});
				
	return false;

}

//資料請求
//カート操作プラス、マイナス（複数霊園対応）
function updateCartMulti(id_list,add_only,del_only) {
	
	var data = new Object();
	
	data.id_list = id_list;
	data.add_only = add_only;
	data.del_only = del_only;

	$.ajax({
		url:'/common_lib/getCartListMulti.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[2]));

		},
			error:function(res, status, e) {
		}
	});
				
	return false;

}


//資料請求
//カート操作プラス、マイナス

function updateCart(product_id,add_only,del_only,del_all,top_regist_no_remake) {
	
	var data = new Object();
	
	data.pid = product_id;
	data.add_only = add_only;
	data.del_only = del_only;
	data.del_all = del_all;

	$.ajax({
		url:'/common_lib/getCartList.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[3]));
			
			
			if(del_all=="1"){
				//資料請求リストを空にするを行った場合

				//PC上部資料請求リスト
				if($("#regist_cemetery_list_cnt").size()>0){
					$("#regist_cemetery_list_cnt").html(decodeURIComponent(results[2]));
				}

				if($("#regist_cemetery_list_cnt2").size()>0){
					$("#regist_cemetery_list_cnt2").html(decodeURIComponent(results[2]));
				}
				
				//削除したリストから存在する要素の表示を切り替える
				var array_del_list = new Array();
				array_del_list = decodeURIComponent(results[4]).split("|");
				
				//window.alert(decodeURIComponent(results[5]));
				
				for (var i=0 ; i<array_del_list.length ; i++){
  					if(array_del_list[i] != ""){
						//window.alert(array_del_list[i]);
	
						//チェックボックスが存在する場合は設定（PC一覧ページ）（ピックアップ:top）
						if(0 < $("#cart_check_area_pickup_top"+array_del_list[i]).size() && (0 >= $("#cart_cnt").size())){
							//存在する
							$("#cart_check_area_pickup_top"+array_del_list[i]).css('background','#EAEAEA');
							$("#cart_check_area_pickup_top"+array_del_list[i]).children('input').prop({'checked':false});
							$("#cart_check_area_pickup_top"+array_del_list[i]).children('img').attr("src","/search/img/img_checkbox2_off.png");
						}else{
							//存在しない
						}

                        //チェックボックスが存在する場合は設定（PC一覧ページ）（ピックアップ:bottom）
                        if(0 < $("#cart_check_area_pickup_bottom"+array_del_list[i]).size() && (0 >= $("#cart_cnt").size())){
                            //存在する
                            $("#cart_check_area_pickup_bottom"+array_del_list[i]).css('background','#EAEAEA');
                            $("#cart_check_area_pickup_bottom"+array_del_list[i]).children('input').prop({'checked':false});
                            $("#cart_check_area_pickup_bottom"+array_del_list[i]).children('img').attr("src","/search/img/img_checkbox2_off.png");
                        }else{
                            //存在しない
                        }
	
						//チェックボックスが存在する場合は設定（PC一覧ページ）
						if(0 < $("#cart_check_area"+array_del_list[i]).size()){
							//存在する
							$("#cart_check_area"+array_del_list[i]).css('background','#EAEAEA');
							$("#cart_check_area"+array_del_list[i]).children('input').prop({'checked':false});
							$("#cart_check_area"+array_del_list[i]).children('img').attr("src","/search/img/img_checkbox2_off.png");
						}else{
							//存在しない
						}
	
						//検討中リストのボタンが存在する場合は設定（詳細ページ）
						if(0 < $("#examination_btn"+array_del_list[i]).size()){
							//存在する
							$("#examination_btn"+array_del_list[i]).attr("src","/search/img/btn-list.png");
						}else{
							//存在しない
						}
	
						//検討中リストのテキストが存在する場合は設定（詳細ページ）
						if(0 < $("#detail_ex_text").size()){
							//存在する
							$("#detail_ex_text").html("検討リストに追加する");
						}else{
							//存在しない
						}
		
						//検討中リストのボタンが存在する場合は設定（詳細ページ）下
						if(0 < $("#examination_btn-bottom-"+array_del_list[i]).size()){
							//存在する
							$("#examination_btn-bottom-"+array_del_list[i]).attr("src","/search/img/btn-list.png");
						}else{
							//存在しない
						}
		
						//この霊園を選んだ方は以下の霊園・墓地も見ています のボタンを変更
						if(0 < $("#seikyu_btn"+array_del_list[i]).size()){
							//存在する
							$("#seikyu_btn"+array_del_list[i]).attr("src","/detail/img/btn-plus.gif");
						}else{
							//存在しない
						}
						
						//現在地周辺(10km圏内)の霊園・墓地 のGoogleバルーン内を変更
						if(0 < $('#iframe'+array_del_list[i]).contents().find("#gex_link_"+array_del_list[i]).size()){
							//存在する
							$('#iframe'+array_del_list[i]).contents().find("#gex_link_"+array_del_list[i]).html("資料請求に追加");
						}else{
							//存在しない
						}

					}
				}

			}else{

				if(decodeURIComponent(results[1])=="3"){
					//削除した場合 -- 3

					//駅近・アクセス至便な霊園特集用
					if($('#access_tokusyu').size()){
						$("#cart_btn_"+product_id).attr("src","/access_tokusyu/img/btn-add.png");
						$("#cart_btn_"+product_id).attr("alt","資料請求リストに追加");
						$("#cart_btn_map_"+product_id).attr("src","/access_tokusyu/img/btn-map-add.png");
					}
					
					//検討中リストも削除
					//updateExamination(product_id,"","1","");

					//PC上部資料請求リスト
					if($("#regist_cemetery_list_cnt").size()>0){
						$("#regist_cemetery_list_cnt").html(decodeURIComponent(results[2]));
					}

					if($("#regist_cemetery_list_cnt2").size()>0){
						$("#regist_cemetery_list_cnt2").html(decodeURIComponent(results[2]));
					}
					
					//スマホ特集ページ
					if($("#cart_list_cnt").size()>0){
						$("#cart_list_cnt").val(decodeURIComponent(results[2]));
					}

					//スマホ一覧（ピックアップ）
					if(0 < $("#pickup_div_"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
						$("#pickup_div_"+product_id).removeClass("checked");
                        $("#cemetery_div_"+product_id).removeClass("checked");
                        $("#pickup_div_"+product_id).children('span').children('input').prop('checked',false);
                        $("#cemetery_div_"+product_id).children('span').children('input').prop('checked',false);
					}else{
						//存在しない
					}

					//スマホ一覧
					if(0 < $("#cemetery_div_"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
                        $("#pickup_div_"+product_id).removeClass("checked");
						$("#cemetery_div_"+product_id).removeClass("checked");
                        $("#pickup_div_"+product_id).children('span').children('input').prop('checked',false);
                        $("#cemetery_div_"+product_id).children('span').children('input').prop('checked',false);
					}else{
						//存在しない
					}

					//ピックアップ霊園と重複表示があるため
					//チェックボックスが存在する場合は設定（PC一覧ページ）（ピックアップ）
					if(0 < $("#pickup_cart_check_area"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
						if($("#pickup_cart_check_area"+product_id).children('input').prop('checked')){
							$("#pickup_cart_check_area"+product_id).css('background','#EAEAEA');
							$("#pickup_cart_check_area"+product_id).children('input').prop({'checked':false});
							$("#pickup_cart_check_area"+product_id).children('img').attr("src","/search/img/img_checkbox_off.png");
						}
					}else{
						//存在しない
					}

					//ピックアップ霊園と重複表示があるため
					//チェックボックスが存在する場合は設定（PC一覧ページ）
					if(0 < $("#cart_check_area"+product_id).size()){
						//存在する
						if($("#cart_check_area"+product_id).children('input').prop('checked')){
							$("#cart_check_area"+product_id).css('background','#EAEAEA');
							$("#cart_check_area"+product_id).children('input').prop({'checked':false});
							$("#cart_check_area"+product_id).children('img').attr("src","/search/img/img_checkbox2_off.png");
						}
					}else{
						//存在しない
					}

					//チェックボックスが存在する場合は設定（スマホ一覧ページ）（ピックアップ）
					if(0 < $("#pickup_cart_check_area"+product_id).size() && 0 < $("#cart_cnt").size()){
						if($("#pickup_cart_check_area"+product_id).children('input').prop('checked')){
							//チェックボックスの状態を変更
							$("#pickup_cart_check_area"+product_id).css('background','#DFDBD4');
							$("#pickup_cart_check_area"+product_id).children('input').prop({'checked':false});
							$("#pickup_cart_check_area"+product_id).children('img').attr("src","/sp/img/img_checkbox_off.png");
						}
					}else{
						//存在しない
					}

					//チェックボックスが存在する場合は設定（スマホ一覧ページ）
					if(0 < $("#cart_check_area"+product_id).size() && 0 < $("#cart_cnt").size()){
						if($("#cart_check_area"+product_id).children('input').prop('checked')){
							//チェックボックスの状態を変更
							$("#cart_check_area"+product_id).css('background','#DFDBD4');
							$("#cart_check_area"+product_id).children('input').prop({'checked':false});
							$("#cart_check_area"+product_id).children('img').attr("src","/sp/img/img_checkbox_off.png");
						}
					}else{
						//存在しない
					}

				}else{
					//追加した場合 -- 2

					//駅近・アクセス至便な霊園特集用
					if($('#access_tokusyu').size()){
						$("#cart_btn_"+product_id).attr("src","/access_tokusyu/img/btn-added.png");
						$("#cart_btn_"+product_id).attr("alt","資料請求リストに追加済");
						$("#cart_btn_map_"+product_id).attr("src","/access_tokusyu/img/btn-map-added.png");
					}

					//PC上部資料請求リスト
					if($("#regist_cemetery_list_cnt").size()>0){
						$("#regist_cemetery_list_cnt").html(decodeURIComponent(results[2]));
					}

					if($("#regist_cemetery_list_cnt2").size()>0){
						$("#regist_cemetery_list_cnt2").html(decodeURIComponent(results[2]));
					}
					
					//スマホ特集ページ
					if($("#cart_list_cnt").size()>0){
						$("#cart_list_cnt").val(decodeURIComponent(results[2]));
					}

					//検討中リストも追加
					//updateExamination(product_id,"1","","");

					//スマホ一覧（ピックアップ）
					if(0 < $("#pickup_div_"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
						$("#pickup_div_"+product_id).addClass("checked");
                        $("#cemetery_div_"+product_id).addClass("checked");
                        $("#pickup_div_"+product_id).children('span').children('input').prop('checked',true);
                        $("#cemetery_div_"+product_id).children('span').children('input').prop('checked',true);
					}else{
						//存在しない
					}

					//スマホ一覧
					if(0 < $("#cemetery_div_"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
                        $("#pickup_div_"+product_id).addClass("checked");
						$("#cemetery_div_"+product_id).addClass("checked");
                        $("#pickup_div_"+product_id).children('span').children('input').prop('checked',true);
                        $("#cemetery_div_"+product_id).children('span').children('input').prop('checked',true);
					}else{
						//存在しない
					}

					//ピックアップ霊園と重複表示があるため
					//チェックボックスが存在する場合は設定（PC一覧ページ）（ピックアップ）
					if(0 < $("#pickup_cart_check_area"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
						if($("#pickup_cart_check_area"+product_id).children('input').prop('checked')==false){
							$("#pickup_cart_check_area"+product_id).css('background','#FF9999');
							$("#pickup_cart_check_area"+product_id).children('input').prop({'checked':true});
							$("#pickup_cart_check_area"+product_id).children('img').attr("src","/search/img/img_checkbox_on.png");
						}
					}else{
						//存在しない
					}

					//ピックアップ霊園と重複表示があるため
					//チェックボックスが存在する場合は設定（PC一覧ページ）
					if(0 < $("#cart_check_area"+product_id).size() && (0 >= $("#cart_cnt").size())){
						//存在する
						if($("#cart_check_area"+product_id).children('input').prop('checked')==false){
							$("#cart_check_area"+product_id).css('background','#FF9999');
							$("#cart_check_area"+product_id).children('input').prop({'checked':true});
							$("#cart_check_area"+product_id).children('img').attr("src","/search/img/img_checkbox2_on.png");
						}
					}else{
						//存在しない
					}

					//チェックボックスが存在する場合は設定（スマホ一覧ページ）（ピックアップ）
					if(0 < $("#pickup_cart_check_area"+product_id).size() && 0 < $("#cart_cnt").size()){
						if($("#pickup_cart_check_area"+product_id).children('input').prop('checked')==false){
							//チェックボックスの状態を変更
							$("#pickup_cart_check_area"+product_id).css('background','#588E6D');
							$("#pickup_cart_check_area"+product_id).children('input').prop({'checked':'checked'});
							$("#pickup_cart_check_area"+product_id).children('img').attr("src","/sp/img/img_checkbox_on.png");	
						}
					}else{
						//存在しない
					}

					//チェックボックスが存在する場合は設定（スマホ一覧ページ）
					if(0 < $("#cart_check_area"+product_id).size() && 0 < $("#cart_cnt").size()){
						if($("#cart_check_area"+product_id).children('input').prop('checked')==false){
							//チェックボックスの状態を変更
							$("#cart_check_area"+product_id).css('background','#588E6D');
							$("#cart_check_area"+product_id).children('input').prop({'checked':'checked'});
							$("#cart_check_area"+product_id).children('img').attr("src","/sp/img/img_checkbox_on.png");	
						}
					}else{
						//存在しない
					}

				}
			
			}
			//window.alert(decodeURIComponent(results[3]));
				
			//スマホ用資料請求数
			if(0 < $("#cart_cnt").size()){

				//存在する
				$("#cart_cnt").val(decodeURIComponent(results[2]));
				$("#dsp_cart_cnt").html(decodeURIComponent(results[2]));
				
				//カート内に霊園が存在する場合、ボタンを表示
				if(decodeURIComponent(results[2]) > 0){
					$.fixedUI('#regist_area');
				}else{
					$.unfixedUI('#regist_area');
				}
			}else{
				//存在しない
			}

		},
			error:function(res, status, e) {
		}
	});
	
	if(top_regist_no_remake=="1"){
	}else{
		regist_cemetery_list_html_pc();
	}
				
	return false;

}

//一覧で資料請求ボタンをクリック時
function regist_link() {
	var data = new Object();
	
	$.ajax({
		url:'/area_list/LGC_CheckCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			if(decodeURIComponent(results[1])=="1"){
				window.alert("霊園・墓地物件を最低１つは選択してください");
			}else{
				if(decodeURIComponent(results[2])<="0"){
					window.alert("資料請求可能な霊園・墓地物件を最低１つは選択してください");
				}else{
					if ( document.domain.indexOf('kamastg') != -1) {
						location.href = 'https://'+document.domain+'/regist/';
					}else{
						location.href = 'https://'+document.domain+'/regist/';
					}
				}
			}

		},
		error:function(res, status, e) {
		}
	});
	
}

//PC一覧で見学予約ボタンをクリック時
function reserve_link() {
	var data = new Object();
	
	$.ajax({
		url:'/area_list/LGC_CheckCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			if(decodeURIComponent(results[1])=="1"){
				window.alert("霊園・墓地物件を最低１つは選択してください");
			}else{
				if ( document.domain.indexOf('kamastg') != -1) {
					location.href = 'https://'+document.domain+'/reserve/';
				}else{
					location.href = 'https://'+document.domain+'/reserve/';
				}
			}

		},
		error:function(res, status, e) {
		}
	});
	
}

//PC一覧でフッターの資料請求ボタン表示
function dspPCListCartBtn() {
	var data = new Object();
	
	$.ajax({
		url:'/area_list/LGC_CheckCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[1]));
			
			if(0 < $("#fix-arealist-footer").size()){
				if(decodeURIComponent(results[1])=="1"){
					$("#fix-arealist-footer").hide();
					$("#fix-arealist-footer").css('display', 'none');
				}else{
					$("#fix-arealist-footer").show();
					//$("#fix-arealist-footer").css('display', 'none');
				}
			}

		},
		error:function(res, status, e) {
		}
	});
	
}

//一覧で資料請求ボタンをクリック時（スマホ用）
function regist_link_sp() {
	var data = new Object();
	
	$.ajax({
		url:'/area_list/LGC_CheckCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			if(decodeURIComponent(results[1])=="1"){
				window.alert("霊園・墓地物件を最低１つは選択してください");
			}else{
				if(decodeURIComponent(results[2])<="0"){
					window.alert("資料請求可能な霊園・墓地物件を最低１つは選択してください");
				}else{
					if ( document.domain.indexOf('kamastg') != -1) {
						location.href = 'https://'+document.domain+'/sp/regist/';
					}else{
						location.href = 'https://'+document.domain+'/sp/regist/';
					}
				}
			}

		},
		error:function(res, status, e) {
		}
	});
	
}

//一覧で見学予約ボタンをクリック時（スマホ用）
function reserve_link_sp() {
	var data = new Object();
	
	$.ajax({
		url:'/area_list/LGC_CheckCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			if(decodeURIComponent(results[1])=="1"){
				window.alert("霊園・墓地物件を最低１つは選択してください");
			}else{
				if ( document.domain.indexOf('kamastg') != -1) {
					location.href = 'https://'+document.domain+'/sp/reserve/';
				}else{
					location.href = 'https://'+document.domain+'/sp/reserve/';
				}
			}

		},
		error:function(res, status, e) {
		}
	});
	
}

//現地見学
//カート操作プラス、マイナス
function updateCartVisit(product_id,add_only,del_only) {
	
	var data = new Object();
	
	data.pid = product_id;
	data.add_only = add_only;
	data.del_only = del_only;

	$.ajax({
		url:'/common_lib/getCartVisitList.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[2]));
			
			if(decodeURIComponent(results[1])=="3"){
				//削除した場合 -- 3
				
			}else{
				//追加した場合 -- 2
				
			}
			//window.alert(decodeURIComponent(results[3]));

		},
			error:function(res, status, e) {
		}
	});
				
	return false;

}

//この霊園を選んだ方は以下の霊園・墓地も見ていますの全チェック（スマホ）
function with_other_cemetery_all_check(all_check_item) {
  
	if($(all_check_item).prop('checked')==true){
		$("input[name='with_other_cemetery_id[]']").prop('checked', true);
	}else{
		$("input[name='with_other_cemetery_id[]']").prop('checked', false);
	}
	
	var sv_cart_list = "";
	$("[name='with_other_cemetery_id[]']").each(function(index){
		if(sv_cart_list==""){
			sv_cart_list = "|"+$(this).val()+"|";
		}else{
			sv_cart_list += $(this).val()+"|";
		}
	});
	
	var data = new Object();
	data.cart_list = sv_cart_list;
	if($(all_check_item).prop('checked')==true){
		data.cstatus = "1";
	}else{
		data.cstatus = "2";
	}
	
	$.ajax({
		url:'/common_lib/LGC_CollectCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[0]));

		},
		error:function(res, status, e) {
		}
	});
	
} 

//この霊園を選んだ方は以下の霊園・墓地も見ていますの全チェッククリック時（スマホ）
//シルバーエッグ送信用
function with_other_all_check_send(all_check_item,utma) {
	
	//全チェックをチェックされた場合のみ処理
	if($(all_check_item).prop('checked')==true){
	
		var sv_cart_list = "";
		var sv_with_other_rqid = "";
		$("[name='with_other_cemetery_id[]']").each(function(index){
			if(sv_cart_list==""){
				sv_cart_list = "|"+$(this).val()+"|";
				if(sv_with_other_rqid==""){
					sv_with_other_rqid = "|"+$("with_other_rqid_"+$(this).val()).val()+"|";
				}else{
					sv_with_other_rqid += $("with_other_rqid_"+$(this).val()).val()+"|";
				}
			}else{
				sv_cart_list += $(this).val()+"|";
				if(sv_with_other_rqid==""){
					sv_with_other_rqid = "|"+$("with_other_rqid_"+$(this).val()).val()+"|";
				}else{
					sv_with_other_rqid += $("with_other_rqid_"+$(this).val()).val()+"|";
				}
			}
		});

		var data = new Object();
		data.prod_list = sv_cart_list;
		data.spec = 'sp911';
		data.utma = utma;
		data.cref_list = sv_with_other_rqid;

		$.ajax({
			url:'/area_list/clickRecommendMulti.php',
			type:'POST',
			data:data,
			dataType:'text',
			async:false,
			success:function(res_json) {

				results = res_json.split(",");
				
				//window.alert(decodeURIComponent(results[1]));

			},
			error:function(res, status, e) {
			}
		});
	}
}

//おすすめの霊園の全チェック（スマホ）
function osusume_all_check(all_check_item) {
  
	if($(all_check_item).prop('checked')==true){
		$("input[name='osusume_cemetery_id[]']").prop('checked', true);
	}else{
		$("input[name='osusume_cemetery_id[]']").prop('checked', false);
	}
	
	var sv_cart_list = "";
	$("[name='osusume_cemetery_id[]']").each(function(index){
		if(sv_cart_list==""){
			sv_cart_list = "|"+$(this).val()+"|";
		}else{
			sv_cart_list += $(this).val()+"|";
		}
	});
	
	var data = new Object();
	data.cart_list = sv_cart_list;
	if($(all_check_item).prop('checked')==true){
		data.cstatus = "1";
	}else{
		data.cstatus = "2";
	}
	
	$.ajax({
		url:'/common_lib/LGC_CollectCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[0]));

		},
		error:function(res, status, e) {
		}
	});
	
} 

//スマホ詳細でフッターの資料請求ボタン表示（スマホ）
function dspSPFootCartBtn() {
	var data = new Object();
	
	$.ajax({
		url:'/area_list/LGC_CheckCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[1]));
			
			if(0 < $("#df-fixbtn").size()){
				if(decodeURIComponent(results[1])=="1"){
					if(0 < $("#fix-content").size()){
						$("#fix-content").show();
					}
					
					if(0 < $("#fix-content1").size()){
						$("#fix-content1").show();
					}
					$("#df-fixbtn").hide();
				}else{
					if(0 < $("#fix-content").size()){
						$("#fix-content").hide();
					}
					
					if(0 < $("#fix-content1").size()){
						$("#fix-content1").hide();
					}
					$("#df-fixbtn").show();
					//$("#fix-arealist-footer").css('display', 'none');
				}
			}

		},
		error:function(res, status, e) {
		}
	});
	
}

//-------------------------------------------------------------
//  他サイトのcookie変更
//-------------------------------------------------------------
function other_site_cookie_update(){

	if($("#site_id").val()=="e-eitai") {
		var data = new Object();

		data.cid_list = "1389237267-402599";

		$.ajax( {
			url: 'http://www.e-eitai.com/common_lib/LGC_AddCookie.php',
			type: 'POST',
			data:data,
			xhrFields: {
				withCredentials: true
			},
			success: function( data ) {
				console.log('通信に成功しました。');
			},
			error: function( data ) {
				console.log('通信に失敗しました。');
			}
		} );
	}
}

//-------------------------------------------------------------
//  他サイトのcookie削除
//-------------------------------------------------------------
function other_site_cookie_dell(){

	if($("#site_id").val()=="e-eitai") {

		$.ajax( {
			url: 'http://www.e-eitai.com/common_lib/LGC_DellCookie.php',
			type: 'POST',
			xhrFields: {
				withCredentials: true
			},
			success: function( data ) {
				console.log('通信に成功しました。');
			},
			error: function( data ) {
				console.log('通信に失敗しました。');
			}
		} );
	}
}
