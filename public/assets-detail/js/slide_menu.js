// JavaScript Document

$(document).ready(function () {
	var scrollpos;
	
	$('#slide_menu').sidr({
		side: 'right',
		onOpen: function(){
			$("body").css("overflow", "hidden");
			//$(window).on('touchmove.noScroll', function(e) {
			//	e.preventDefault();
			//});
			
			scrollpos = $(window).scrollTop();
			$('body').addClass('fixed').css({'top': -scrollpos});
			
			$("#main_overlay").show();
			$("#main-menu").show();
			dataLayer.push({
			'eventAction': 'MENU',
			'event': 'MENU Open'
			});
		},
		onClose: function(){
			$("body").css("overflow", "auto");
			/*$(window).off('.noScroll');*/
			
			$('body').removeClass('fixed').css({'top': 0});
      		window.scrollTo( 0 , scrollpos );
			
			$("#main_overlay").hide();
			$("#main-menu").hide();
		}
	});
	
	$("#main_overlay").on("click", function() {
		$.sidr('close', 'sidr');
	});
});

$( window ).resize(function () {
	$.sidr('close', 'sidr');
});
