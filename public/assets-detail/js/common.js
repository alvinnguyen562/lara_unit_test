// JavaScript Document

//初期読み込み
$(function() {
	$("body").append('<div class="fix-footer" id="fix-content2" style="display:none;padding:0px;"><div class="ttl-block"><span class="ttl">お問い合わせ・ご相談</span><span class="close"><a href="javascript:void(0);" onClick="javascript:$(\'.ui-dialog-content\').closest(\'.ui-dialog-content\').dialog(\'close\');">▼ 閉じる</a></span></div><div class="tel-block"><a href="javascript:void(0);"><img src="/sp/detail/img/fix-footer-tel.png" alt="お電話でのお問い合わせ タッチしてお電話ください 0120-949-938 無料相談・年中無休 受付時間7:00~24:00" onclick="javascript:location.href=\'tel:0120949938\';"></a></div><div class="btn-block"></div></div>');
});

//固定フッターの表示
function DistFixFootter(cemetery_id,toriatsukai_flg) {
  
	var regist_btn = "";
	var reserve_btn = "";
	
	if (toriatsukai_flg=="1") {
		regist_btn = '<div class="btn request"><a href="javascript:void(0);" onClick="javascript:updateCart(\''+cemetery_id+'\',\'1\');location.href=\'https://www.e-ohaka.com/sp/regist/\';"><img src="/sp/detail/img/fix-footer-request.png" alt="資料請求(無料)60秒で入力完了"><br>複数霊園一括請求可能!</a></div>';
	}
	
	if (toriatsukai_flg=="1" || toriatsukai_flg=="3") {
		reserve_btn = '<div class="btn campaign"><a href="javascript:void(0);" onClick="javascript:updateCart(\''+cemetery_id+'\',\'1\');location.href=\'https://www.e-ohaka.com/sp/reserve/\';"><img src="/sp/detail/img/fix-footer-campaign.png" alt="見学予約キャンペーン実施中！"><br>ギフトカード3,000円プレゼント!</a></div>';
	}
	
	$('<div class="fix-footer" id="fix-content2" style="display:none;padding:0px;"><div class="ttl-block"><span class="ttl">お問い合わせ・ご相談</span><span class="close"><a href="javascript:void(0);" onClick="javascript:$(\'.ui-dialog-content\').closest(\'.ui-dialog-content\').dialog(\'close\');">▼ 閉じる</a></span></div><div class="tel-block"><a href="javascript:void(0);"><img src="/sp/detail/img/fix-footer-tel.png" alt="お電話でのお問い合わせ タッチしてお電話ください 0120-949-938 無料相談・年中無休 受付時間7:00~24:00" onclick="javascript:location.href=\'tel:0120949938\';"></a></div><div class="btn-block">' + regist_btn + reserve_btn + '</div></div>').dialog({
        width: "100%",
		create: function() {
    	},
		dialogClass: 'noTitleDialog',
		modal: true,
		resizable: false,
		position:['center','bottom'],
		async:false,
		open: function(){
			$('#fix-content1').hide();
			$("body").css("overflow", "hidden");
			$(window).on('touchmove.noScroll', function(e) {
    e.preventDefault();
});
		},
		close: function(){
			$('#fix-content1').show();
			$("body").css("overflow", "auto");
			$(window).off('.noScroll');
		}
	});

} 

//補足情報の開閉
function table_other_open(line_no) {
  
	if ($('#table_other_'+line_no).is(':visible')) {
		$('#table_other_'+line_no).hide();
	}else{
		$('#table_other_'+line_no).show();
	}

} 
