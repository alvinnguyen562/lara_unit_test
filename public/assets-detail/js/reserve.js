function click_reserve_btn(id) {
	updateCart(id,"1");
	location.href = '/request/';
}

function click_reserve_btn_sp(id) {
	updateCart(id,"1");
	//location.href = '/sp/request/';
	window.document.resultList.method = "post";
	document.resultList.submit();
}

//カート操作プラス、マイナス
function updateReserve(product_id,add_only,del_only) {
	
	var data = new Object();
	
	data.pid = product_id;
	data.add_only = add_only;
	data.del_only = del_only;

	$.ajax({
		url:'/common_lib/getReserveList.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			if(decodeURIComponent(results[1])=="3"){
				//削除した場合 -- 3

			}else{
				//追加した場合 -- 2

			}
			//window.alert(decodeURIComponent(results[3]));
			//要素の存在チェックを行って設定
			if(0 < $("#reserve_cnt").size()){
				//alert("OK");
				$("#reserve_cnt").val(decodeURIComponent(results[2]));
			}
			if(0 < $("#dsp_reserve_cnt1").size()){
				//alert("OK");
				$("#dsp_reserve_cnt1").html(decodeURIComponent(results[2]));
			}
			if(0 < $("#dsp_reserve_cnt2").size()){
				//alert("OK");
				$("#dsp_reserve_cnt2").html(decodeURIComponent(results[2]));
			}

		},
			error:function(res, status, e) {
		}
	});
				
	return false;

}

//まとめてチェック
function checkAll(this_object,f) {

	if($(this_object).hasClass("checked")){
		var chk_cnt = 0;
		var sv_res_id = "";
		for(var i=0;i<f.elements["reserve_check[]"].length;i++){
	
			//window.alert(f.elements["reserve_check[]"][i].value);
			sv_res_id = f.elements["reserve_check[]"][i].value;
			$('#reserve_check_img'+sv_res_id).addClass('checked');
			$('#reserve_check_img'+sv_res_id).find('input').attr('checked','checked');
			
			updateCart(sv_res_id,"1","");
	
		}
		
		$('#matomete_check_img1').addClass('checked');
		$('#matomete_check_img1').find('input').attr('checked','checked');
		$('#matomete_check_img2').addClass('checked');
		$('#matomete_check_img2').find('input').attr('checked','checked');
	}else{
		var chk_cnt = 0;
		var sv_res_id = "";
		for(var i=0;i<f.elements["reserve_check[]"].length;i++){
	
			//window.alert(f.elements["reserve_check[]"][i].value);
			sv_res_id = f.elements["reserve_check[]"][i].value;
			$('#reserve_check_img'+sv_res_id).removeClass('checked');
			$('#reserve_check_img'+sv_res_id).find('input').removeAttr('checked','checked');
			
			updateCart(sv_res_id,"","1");
	
		}
		
		$('#matomete_check_img1').removeClass('checked');
		$('#matomete_check_img1').find('input').removeAttr('checked','checked');
		$('#matomete_check_img2').removeClass('checked');
		$('#matomete_check_img2').find('input').removeAttr('checked','checked');
	}

}
