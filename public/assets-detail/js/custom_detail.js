$(document).ready(function() {
	$(".lazy").lazyload({
		effect: "fadeIn",
		effectspeed: 10
	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > 10) {
			$('#fix-content-footer').show();
		} else {
			$('#fix-content-footer').hide();
		}
	});

	//お気に入り追加ウィンドウ
	$('#dialog_favorite_add').dialog({
        autoOpen: false,
        modal: true,
		resizable: false,
		position:['center','center'],
        buttons: {
            // 「OK」ボタンがクリックされた時には、
            // ダイアログボックスを閉じる
            'OK': function() {
				$(this).dialog('close');
            }
        }
    });
	
	$(document).on("click", ".ui-widget-overlay", function(){
		if($('#dialog_favorite_add').dialog('isOpen')){
			$('#dialog_favorite_add').dialog('close');
		}
		if($('#dialog_favorite_del').dialog('isOpen')){
			$('#dialog_favorite_del').dialog('close');
		}
	});
	
	$(window).on('scroll', function(event){
		if($('#dialog_favorite_add').dialog('isOpen')){
			$('#dialog_favorite_add').dialog('close');
		}
		if($('#dialog_favorite_del').dialog('isOpen')){
			$('#dialog_favorite_del').dialog('close');
		}
	});

	//お気に入り削除ウィンドウ
	$('#dialog_favorite_del').dialog({
        autoOpen: false,
        modal: true,
		resizable: false,
		position:['center','center'],
        buttons: {
            // 「OK」ボタンがクリックされた時には、
            // ダイアログボックスを閉じる
            'OK': function() {
				$(this).dialog('close');
            }
        }
    });
});

//お気に入り追加ウィンドウを開く
function open_dialog_favorite_add(){
	//$('#dialog_favorite_add').dialog('open');
	$('#dialog_favorite_add').parent().css({position:"fixed"}).end().dialog('open');
}

//お気に入り削除ウィンドウを開く
function open_dialog_favorite_del(){
	//$('#dialog_favorite_del').dialog('open');
	$('#dialog_favorite_del').parent().css({position:"fixed"}).end().dialog('open');
}

//お気に入りCookie設定
function contFavoriteCookie(cont_status,cemetery_id) {
  
  // Ajax通信
	var data = new Object();
	data.cont_status = cont_status;
	data.cemetery_id = cemetery_id;
	
	$.ajax({
		url:'/common/FavoriteControl.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		modal : true,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			if(cont_status=='del'){
				element = document.getElementById('favorite_atag');
  				element.onclick = new Function("contFavoriteCookie('add','"+cemetery_id+"');");
				
				$("#favorite_img").attr("src","/sp/detail/img/btn_favorite_s.png");
				//window.alert('del-->'+decodeURIComponent(results[1]));
				open_dialog_favorite_del();
			}else{
				element = document.getElementById('favorite_atag');
  				element.onclick = new Function("contFavoriteCookie('del','"+cemetery_id+"');");
				
				$("#favorite_img").attr("src","/sp/detail/img/btn_favorite_cuts.png");
				//window.alert('add');
				open_dialog_favorite_add();
			}

		},
		error:function(res, status, e) {
		}
	});

}