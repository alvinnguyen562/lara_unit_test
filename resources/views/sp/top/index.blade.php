<!DOCTYPE html>
<html lang="ja">
@include('sp.partials.head')
<body>
    @inject('commonService', 'App\Services\CommonService')
    {!! $commonService->dispHead('logo') !!}
    <div id="fix-content" style="display: none;">
        <a href="https://www.e-ohaka.com/sp/remodel/contact/">
            <div id="box-easy">
                <p class="txt-easy">入力たったの1分</p>
                <p>簡単1分!資料請求</p>
            </div>
        </a>
        <a href="tel:0120949938">
            <div id="footer_tel_top">
                <p class="caption">タッチしてお電話ください</p>
                <div class="tel">
                    <p class="txt-tel"><img src="assets/img/icon_fd.png" alt="電話" class="icon-tel">0120-949-938</p>
                </div>
                <p class="lead">年中無休 7:00～24:00</p>
            </div>
        </a>
    </div>
        <!--/#header -->
    <main role="main">

        <div class="area-lead-new">
            <p class="box-count">掲載霊園数<span>7,747</span>件</p>
            <p class="txt-lead">全国の霊園・墓地を探す</p>
        </div>

        <p class="txt-tv">NHK「あさイチ」で紹介されました！</p>

        <ul class="area-search-btn">
            <li class="btn-area"><h3><a href="https://www.e-ohaka.com/sp/area/">地域<br>
                <span>から探す</span></a></h3></li>
            <li class="btn-line"><h3><a href="https://www.e-ohaka.com/sp/line/">路線<br>
                <span>から探す</span></a></h3></li>
        </ul>

        <div class="area-search-plus">
        </div>

        <section role="search" id="search-new">
            <div class="area-keyword">
                <div class="keyword-serch">
                <form action="https://www.e-ohaka.com/sp/result/" id="cse-search-box" class="search-form">
                    <input type="hidden" name="cx" value="018108133237236264779:qc9jsglkjcu">
                    <input type="hidden" name="cof" value="FORID:10;NB:1">
                    <input type="hidden" name="hl" value="ja">
                    <input type="hidden" name="ie" value="utf-8">
                    <input type="hidden" name="oe" value="utf-8">
                    <input type="text" name="q" id="q" class="keywords" placeholder="検索で探す">
                    <button type="button" onclick="javascript:if($(&#39;#search_wrd&#39;).val()==&#39;&#39;){window.alert(&#39;キーワードを入力してください。&#39;)}else{document.search-form.submit();}" class="btn-kensaku">検索</button>
                </form>
                </div>
            </div>

            <div><a href="https://www.e-ohaka.com/sp/vr-reserve/"><img src="assets/img/vr_sp_bn.jpg" alt="VRで100霊園まとめて見学！"></a></div>



            <h2 class="ttl-new">特集から探す</h2><ul class="list-toku"><li><a href="https://www.e-ohaka.com/sp/osusume_tokyo/" target="_self"><img src="assets/img/1514257511-216128.jpg" alt="東京都オススメの霊園" width="362" class="icon-img"><p>スタッフがおススメする<br>霊園10選</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/gardening.html" target="_self"><img src="assets/img/1514257539-678455.jpg" alt="花咲く人気のガーデニング霊園特集" width="362" class="icon-img"><p>花咲く人気の<br>ガーデニング霊園特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/noukotsudou.html" target="_self"><img src="assets/img/1514257598-175549.jpg" alt="天候に左右されない屋内納骨堂特集" width="362" class="icon-img"><p>天候に左右されない<br>屋内納骨堂特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/jyumokusou.html" target="_self"><img src="assets/img/1514257626-621474.jpg" alt="自然に還る樹木葬特集" width="362" class="icon-img"><p>自然に還る<br>樹木葬特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/eitaikuyou.html" target="_self"><img src="assets/img/1514257654-725260.jpg" alt="永続的に管理・供養してもらえる永代供養墓特集" width="362" class="icon-img"><p>永続的に管理・供養してもらえる<br>永代供養墓特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/withpet.html" target="_self"><img src="assets/img/1514257683-895808.jpg" alt="ペットと眠る霊園特集" width="362" class="icon-img"><p>ペットと眠る<br>霊園特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/detail/id1506484231-143576.html" target="_self"><img src="assets/img/1514257907-168823.jpg" alt="東京御廟本館" width="362" class="icon-img"><p>町屋駅より徒歩3分。先進納骨堂38万円～<br>東京御廟　本館</p></a></li></ul>
            <h2 class="ttl-new">特色・こだわりから探す</h2>

            <ul class="popular clearfix">
                <li class="list04"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f4.html">ペットと一緒<br>
                    に入れる</a></li>
                <li class="list05"><a href="https://www.e-ohaka.com/sp/area_list/categorycategoryx/f13.html">ガーデニング<br>
                    霊園</a></li>
                <li class="list06"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f7.html">駅近<br>
                    アクセス至便</a></li>
                <li class="list08"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f14.html">一般墓</a></li>
                <li class="list09"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f9.html">樹木葬</a></li>
                <li class="list10"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f2.html">永代供養墓</a></li>
                <li class="list11"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f3.html">納骨堂</a></li>
                <li class="list07"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f1.html">新規開園</a></li>
                <li class="list13"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/s3.html">寺院墓地</a></li>
                <li class="list12"><a href="https://www.e-ohaka.com/sp/area_list/categcategoryx/s2.html">公営霊園</a></li>
                <li class="list14"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f56.html">海・富士山が<br>見える</a></li>
                <li class="list18"><a href="https://www.e-ohaka.com/sp/area_list/categoryx/f12.html">高級霊園</a></li>
                        </ul>
        </section>
        <!--/#search -->
        <section id="area-menu-f">


                <h2 class="ttl-new">サービス・キャンペーン</h2><ul class="list-toku"><li><a href="https://www.e-ohaka.com/boseki_catalog/" target="_blank"><img src="assets/img/1517456787-408676.jpg" alt="墓石のカタログ請求" width="362" class="icon-img"><p>工事含めて定額50万円<br>全国対応！選べる墓石カタログ</p></a></li><li><a href="https://www.e-ohaka.com/sp/benefit/" target="_blank"><img src="assets/img/1514256994-982319.jpg" alt="あんしん＆お得なお墓選び" width="362" class="icon-img"><p>あんしん＆お得なお墓選び<br>「いいお墓」3つのメリットご紹介</p></a></li><li><a href="https://www.e-ohaka.com/sp/campaign2/" target="_blank"><img src="assets/img/1514257049-961250.jpg" alt="霊園の現地見学キャンペーン" width="362" class="icon-img"><p>霊園の現地見学キャンペーン<br>ギフトカード3000円分プレゼント！</p></a></li><li><a href="https://www.e-ohaka.com/sp/eitaiguide2015/" target="_blank"><img src="assets/img/1514257090-031243.jpg" alt="永代供養墓ガイド2015決定版" width="362" class="icon-img"><p>永代供養墓ガイド（一都三県版）<br>無料プレゼント中！</p></a></li><li><a href="https://www.e-ohaka.com/sp/remodel/contact/" target="_blank"><img src="assets/img/1514257132-251548.jpg" alt="お墓のプロがあなたにあった霊園をご紹介します。「簡単一発」霊園選び" width="362" class="icon-img"><p>あなたに合った霊園の資料をお届け‼<br>簡単1分！資料請求</p></a></li><li><a href="https://www.e-ohaka.com/sp/remodel/move/" target="_blank"><img src="assets/img/1514257177-356149.jpg" alt="お墓の引越し（改葬・移設）" width="362" class="icon-img"><p>お墓の引越し（改葬・移設）<br>簡単一括見積り</p></a></li><li><a href="https://www.e-ohaka.com/sp/campaign/" target="_blank"><img src="assets/img/1514257219-615417.jpg" alt="商品券1万円プレゼントキャンペーン実施中！" width="362" class="icon-img"><p>霊園ご購入⇒口コミ評価で<br>ギフトカード10000円分プレゼント！</p></a></li><li><a href="https://www.e-ohaka.com/sp/shindan/" target="_blank"><img src="assets/img/1514257283-773484.jpg" alt="あなたにピッタリのお墓をご提案" width="362" class="icon-img"><p>あなたにピッタリのお墓をご提案<br>お墓診断！（ベータ版）</p></a></li></ul>    </section>


                <section class="section">
                    <h2 class="ttl-osusume mb5">よく選ばれている霊園・墓地</h2>

                <ul class="list-osusume">
                    @foreach($listCemeteries as $value)
                    <a href="https://www.e-ohaka.com/sp/detail/id1203477483-903050.html" class="recommend_title" data-link-shop="都立　小平霊園" data-link-type="" onclick="javascript:ClickRecoomendItem(&#39;1203477483-903050&#39;,&#39;sp111&#39;,&#39;424047735&#39;);" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
                        <li>
                            <div class="box-detail">
                                <h5 class="ttl-reien">都立　小平霊園</h5>
                                <div class="box-inner">
                                    <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1203477483-903050_1.jpg" data-link-shop="都立　小平霊園" data-link-type="" alt="都立　小平霊園" style="width: 478px; max-width: 100%; display: inline;" src="assets/img/1203477483-903050_1.jpg"></div>
                                    <div style="width:60%;">
                                        <p class="txt-price">4.1万円</p>
                                        <p class="txt-price"><img src="assets/img/stars_45.gif" style="width:70px;height:auto;">&nbsp;4.6</p>
                                        <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">{{ $value->CEMETERY_NAME }}</p>

                                        <ul class="list-icons"><li class="icon-eki">駅近</li></ul>

                                        <p>
                                            </p><div onclick="javascript:updateCart(&#39;1203477483-903050&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </a>
                    @endforeach
                </ul>

                </section>

        <section class="section">
            <h2 class="ttl-osusume mb5">おすすめの霊園</h2>

            <ul class="list-osusume">


        <section class="sec-rec">
            <ul style="margin:0px;">

            <a href="https://www.e-ohaka.com/sp/detail/id1352968378-394747.html" class="recommend_title" data-link-shop="天妙国寺　永代供養納骨堂　鳳凰堂" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">天妙国寺　永代供養納骨堂　鳳凰堂</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1352968378-394747_1.jpg" data-link-shop="天妙国寺　永代供養納骨堂　鳳凰堂" data-link-type="" alt="天妙国寺　永代供養納骨堂　鳳凰堂" style="width: 478px; max-width: 100%; display: inline;" src="assets/img/1352968378-394747_1.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">50.0万円～</p>
                                <p class="txt-price"><img src="assets/img/stars_45.gif" style="width:70px;height:auto;">&nbsp;4.5</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都品川区</p>

                                <ul class="list-icons"><li class="icon-eki">駅近</li></ul>

                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1352968378-394747&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            <a href="https://www.e-ohaka.com/sp/detail/id1235726548-638630.html" class="recommend_title" data-link-shop="練馬ねむの木ガーデン" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">練馬ねむの木ガーデン</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1235726548-638630_1.jpg" data-link-shop="練馬ねむの木ガーデン" data-link-type="" alt="練馬ねむの木ガーデン" style="width: 478px; max-width: 100%; display: inline;" src="assets/img/1235726548-638630_1.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">48.0万円</p>
                                <p class="txt-price"><img src="assets/img/stars_40.gif" style="width:70px;height:auto;">&nbsp;4.2</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都練馬区</p>

                                <ul class="list-icons"><li class="icon-eki">駅近</li><li class="icon-garden">ガーデニング</li></ul>

                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1235726548-638630&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            <a href="https://www.e-ohaka.com/sp/detail/id1368675979-620313.html" class="recommend_title" data-link-shop="シムティエール外苑の杜" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">シムティエール外苑の杜</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1368675979-620313_5.jpg" data-link-shop="シムティエール外苑の杜" data-link-type="" alt="シムティエール外苑の杜" style="width: 478px; max-width: 100%; display: inline;" src="assets/img/1368675979-620313_5.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">32.8万円～</p>
                                <p class="txt-price"><img src="assets/img/stars_45.gif" style="width:70px;height:auto;">&nbsp;4.7</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都新宿区</p>

                                <ul class="list-icons"><li class="icon-eki">駅近</li><li class="icon-garden">ガーデニング</li><li class="icon-pet">ペット可</li></ul>

                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1368675979-620313&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            <a href="https://www.e-ohaka.com/sp/detail/id1331969831-116969.html" class="recommend_title" data-link-shop="三鷹メモリアルグランデ" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">三鷹メモリアルグランデ</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1331969831-116969_1.jpg" data-link-shop="三鷹メモリアルグランデ" data-link-type="" alt="三鷹メモリアルグランデ" style="width: 478px; max-width: 100%; display: inline;" src="assets/img/1331969831-116969_1.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">65.6万円～</p>
                                <p class="txt-price"><img src="assets/img/stars_40.gif" style="width:70px;height:auto;">&nbsp;4.3</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都三鷹市</p>



                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1331969831-116969&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            <a href="https://www.e-ohaka.com/sp/detail/id1363845554-703709.html" class="recommend_title" data-link-shop="瑠璃光寺　ふれあいの碑" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">瑠璃光寺　ふれあいの碑</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1363845554-703709_1.jpg" data-link-shop="瑠璃光寺　ふれあいの碑" data-link-type="" alt="瑠璃光寺　ふれあいの碑" style="width: 559px; max-width: 100%; display: inline;" src="assets/img/1363845554-703709_1.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">42.0万円</p>
                                <p class="txt-price"><img src="assets/img/stars_40.gif" style="width:70px;height:auto;">&nbsp;4.1</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都港区</p>



                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1363845554-703709&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            <a href="https://www.e-ohaka.com/sp/detail/id1256276167-316740.html" class="recommend_title" data-link-shop="ひので霊園メモリアルガーデンパーク" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">ひので霊園メモリアルガーデンパーク</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1256276167-316740_1.jpg" data-link-shop="ひので霊園メモリアルガーデンパーク" data-link-type="" alt="ひので霊園メモリアルガーデンパーク" style="width: 559px; max-width: 100%; display: inline;" src="assets/img/1256276167-316740_1.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">8.4万円～</p>
                                <p class="txt-price"><img src="assets/img/stars_40.gif" style="width:70px;height:auto;">&nbsp;4.2</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都日の出町</p>

                                <ul class="list-icons"><li class="icon-pet">ペット可</li></ul>

                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1256276167-316740&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            <a href="https://www.e-ohaka.com/sp/detail/id1287558056-595846.html" class="recommend_title" data-link-shop="龍善寺　早稲田納骨堂" data-link-type="" style="text-decoration:none;position: static;padding-right:0px;font-size: 15px;font-weight: 700;color: #118650;">
            <li>
                    <div class="box-detail">
                        <h5 class="ttl-reien">龍善寺　早稲田納骨堂</h5>
                        <div class="box-inner">
                            <div class="thumb" style="width:40%;"><img class="lazy" data-original="assets/img/1287558056-595846_1.jpg" data-link-shop="龍善寺　早稲田納骨堂" data-link-type="" alt="龍善寺　早稲田納骨堂" style="width: 478px; max-width: 100%; display: inline;" src="assets/img/1287558056-595846_1.jpg"></div>
                            <div style="width:60%;">
                                <p class="txt-price">28.0万円～</p>
                                <p class="txt-price"><img src="assets/img/stars_50.gif" style="width:70px;height:auto;">&nbsp;4.7</p>
                                <p style="color:rgb(64,64,64);font-size:13px;font-weight:normal;">東京都新宿区</p>

                                <ul class="list-icons"><li class="icon-eki">駅近</li></ul>

                                <p>
                                    </p><div onclick="javascript:updateCart(&#39;1287558056-595846&#39;,&#39;1&#39;);location.href=&#39;/sp/regist/&#39;;return false;"><img src="assets/img/btn_document.png" alt="資料請求をする" style="width:100%;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
            </li>
            </a>

            </ul>

        </section>

        <section class="area-about">
            <h3 class="title">当サイト「いいお墓」とは？</h3>
            <p class="txt-about">霊園・墓地の掲載数No.1！日本最大級のお墓総合ポータルサイト「いいお墓」。霊園・墓地の資料請求や現地見学及びお墓の選び方などのご相談を専門の相談員が電話やメールで無料で承っています。お墓・永代供養墓・樹木葬の価格（費用）、建墓のポイント、石材店の選び方や改葬、質問集（Q&amp;A）などお墓に関する情報コンテンツ満載。全国の霊園・墓地探しは、鎌倉新書の「いいお墓」にお任せください。</p>
        </section>

            {!! $commonService->dispFootTelBanner() !!}
            <!--/#examination-list -->
    </ul></section>
</main>
    <!--/#main -->


    {!! $commonService->dispFoot() !!}


    <div id="df-fixbtn" class="fix-btn02" style="display:none;">
        <p class="txt"> チェックした物件を（30件まで）</p>
        <div class="linetab">
            <p><a id="fix-btn02_regist_a" href="javascript:void(0);" onclick="javascript:regist_link_sp();return false;"><img id="fix-btn02_regist_img" src="assets/img/btn001.png" alt=""></a></p>
            <p><a id="fix-btn02_reserve_a" href="javascript:void(0);" onclick="javascript:reserve_link_sp();return false;"><img id="fix-btn02_reserve_img" src="assets/img/btn002.png" alt=""></a></p>
        </div>
    </div>

        {!! Html::script('assets/js/jquery-1.10.2.min.js') !!}
        {!! Html::script('assets/js/jquery.lazyload.js') !!}
        {!! Html::script('assets/js/owl.carousel.mi.js') !!}
        {!! Html::script('assets/js/heightLine.js') !!}
        {!! Html::script('assets/js/script.js') !!}
        {!! Html::script('assets/js/getLatLon.js') !!}
        {!! Html::script('assets/js/cart.js') !!}
        {!! Html::script('assets/js/change_siteview.js') !!}
        {!! Html::script('assets/js/modal.js') !!}
        {!! Html::script('assets/js/jquery.sidr.min.js') !!}
        {!! Html::script('assets/js/custom_index.js') !!}
        {!! Html::script('assets/js/slide_menu.js') !!}
        <script type="text/javascript">var smnAdvertiserId = '00006135';</script>
        <script type="text/javascript" src="assets/js/pixel.js"></script><script type="text/javascript" src="https://px.ladsp.com/pixel?advertiser_id=00006135&amp;referer="></script>
        <script type="text/javascript" src="assets/js/ld.js" async=""></script>
        <script type="text/javascript">
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push(
                { event: "setAccount", account: 18210 },
                { event: "setSiteType", type: "m" },
                { event: "viewHome" }
        );
        </script>
        <script type="text/javascript" src="assets/js/conversion_async.js"></script>
    </body>
</html>
