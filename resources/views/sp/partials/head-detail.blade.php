<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>目黒駅前大師堂｜東京都品川区【駅近】｜資料請求・見学予約【いいお墓】</title>
<meta name="description" content="【NHKで紹介されたお墓探しのサイト】駅から徒歩圏内の好立地！東京都品川区にある目黒駅前大師堂はお墓参りに便利な【駅近】の霊園・墓地です。最大1万円分商品券プレゼント中！">

<meta name="keywords" content="目黒駅前大師堂,品川区,23区,東京都,資料請求,見学,相談,キャンペーン">

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, maximum-scale=1.0;">
<meta property="og:title" content="目黒駅前大師堂｜東京都品川区【駅近】｜資料請求・見学予約【いいお墓】">
<meta property="og:description" content="【NHKで紹介されたお墓探しのサイト】駅から徒歩圏内の好立地！東京都品川区にある目黒駅前大師堂はお墓参りに便利な【駅近】の霊園・墓地です。最大1万円分商品券プレゼント中！">
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.e-ohaka.com/">
<meta property="og:image" content="http://www.e-ohaka.com/common_img/og-image.jpg">
<meta property="og:site_name" content="全国のお墓が探せる「いいお墓」">

<link rel="canonical" href="https://www.e-ohaka.com/detail/id1424763353-305329.html">


{!! Html::style('assets/css/modal.css') !!}
{!! Html::style('./assets-detail/css/default.css') !!}
{!! Html::style('./assets-detail/css/common_new.css') !!}
{!! Html::style('./assets-detail/css/detail_new.css') !!}
{!! Html::style('./assets-detail/css/jquery-ui-1.9.2.custom.css') !!}
{!! Html::style('./assets-detail/css/modal.css') !!}
{!! Html::style('./assets-detail/css/jquery.bxslider.css') !!}
{!! Html::style('./assets-detail/css/jquery.sidr.bare.css') !!}
{!! Html::style('./assets-detail/css/regist_add.css') !!}

<style>
.bx-wrapper {
    margin:0 auto 40px;
}
.bx-wrapper .bx-controls-direction a {
    z-index:10;
}
</style>

<style>
.noTitleDialog .ui-dialog-titlebar {display:none;}
</style>

<script type="text/javascript" src="./sp-detail_files/1cbef60b9a"></script><script src="./sp-detail_files/nr-1026.min.js"></script><script type="text/javascript" async="" src="./sp-detail_files/ec.js"></script><script type="text/javascript" async="" src="./sp-detail_files/analytics.js"></script><script async="" charset="utf-8" src="./sp-detail_files/pixel"></script><script async="" src="./sp-detail_files/saved_resource"></script><script src="./sp-detail_files/159349194724091" async=""></script><script async="" src="./sp-detail_files/fbevents.js"></script><script charset="utf-8" async="" src="./sp-detail_files/pixel2_asr.js"></script><script async="" src="./sp-detail_files/gtm.js"></script>
 {!! Html::script('./assets-detail/js/jquery-1.10.2.min.js') !!}
<script>
/*
$(document).ready(function() {
    var $input = $('#refresh');
    $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
});
*/
</script>

 {!! Html::script('./assets-detail/js/slide_menu.js') !!}
{!! Html::script('./assets-detail/js/jquery-ui-1.9.2.custom.js') !!}
{!! Html::script('./assets-detail/js/jquery.lazyload.js') !!}

{!! Html::script('./assets-detail/js/common.js') !!}
{!! Html::script('./assets-detail/js/cart.js') !!}
{!! Html::script('./assets-detail/js/examination.js') !!}

{!! Html::script('./assets-detail/js/reserve.js') !!}
{!! Html::script('./assets-detail/js/modal.js') !!}
{!! Html::script('./assets-detail/js/common_check.js') !!}
{!! Html::script('./assets-detail/js/getAddress.js') !!}

{!! Html::script('./assets-detail/js/jquery.sidr.min.js') !!}
<style>
    #main_overlay {
        display: none;
        width: 100%;
        height: 100%;
        text-align: center;
        position: fixed;
        top: 0;
        z-index: 100;
        background: rgba(0, 0, 0, 0.7);
    }
</style>
<script type="text/javascript" src="./sp-detail_files/slide_menu.js" defer=""></script>

<script type="text/javascript" src="./sp-detail_files/jquery.bxslider.min.js" defer=""></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#bxslider_list').after('<div id="slide-counter" style="position: relative; top: -25px; border-radius: 10px; background-color: #666; width: 50px; color: #FFF;"><span style="margin-left: 13px;"></span>/</div>');
    var slider_config = {
        auto: true,
        pager: true,
        speed: '500',
        pause: '10000',
        pager: false,
        mode: 'horizontal', // place all slider options here
        onSlideBefore: function($slideElement, oldIndex, newIndex){
            var $lazy = $slideElement.find(".lazy")
            var $load = $lazy.attr("data-original");
            $lazy.attr("src",$load).removeClass("lazy");
            $('#slide-counter span').text(newIndex + 1);
        },
        onSliderLoad: function(currentIndex) {
            $('#slide-counter span').text(currentIndex + 1);
        }
    }
    // init the slider with your options
    var slider = $('#bxslider_list').bxSlider(slider_config);
    $('#slide-counter').append(slider.getSlideCount());
});

//レコメンドクリック時の処理
function ClickRecoomendItem(prod, spec, cref) {
    var data = new Object();
    data.prod = prod;
    data.spec = spec;
    data.utma = '1518089971-977200';
    data.cref = cref;

    $.ajax({
        url: '/area_list/clickRecommend.php',
        type: 'POST',
        data: data,
        dataType: 'text',
        async: false,
        success: function(res_json) {

            results = res_json.split(",");

            //window.alert(decodeURIComponent(results[0]));
        },
        error: function(res, status, e) {}
    });
}
</script>

<style>
    .ui-widget-overlay
    {
         cursor: pointer;
    }
</style>
<script type="text/javascript" src="./sp-detail_files/custom_detail.js" defer=""></script>


<script type="text/javascript">
 var smnAdvertiserId = '00006135';
 var smnProductGroupId = '00007736';
 var smnAdvertiserProductId = '1424763353-305329';
</script>
<script type="text/javascript" src="./sp-detail_files/pixel_asr.js"></script><script type="text/javascript" src="./sp-detail_files/pixel(1)"></script>

<script type="text/javascript" src="./sp-detail_files/ld.js" async="true"></script>
<script type="text/javascript">
window.criteo_q = window.criteo_q || [];
window.criteo_q.push(
        { event: "setAccount", account: 18210 },
        { event: "setSiteType", type: "m" },
        { event: "viewItem", item: "1424763353-305329" }
);
</script>
<!-- Googleタグマネージャー用 From -->
<script type="text/javascript">
var dataLayer = [{
'trackPageviewUrl' : '/sp/detail/id1424763353-305329-pa.html',
'detailType': '納骨堂',
'features_code': '永代供養墓-納骨堂-駅近'
}];
</script>
<!-- Googleタグマネージャー用 To -->
<script type="text/javascript" src="./sp-detail_files/conversion_async.js"></script><style id="style-1-cropbar-clipper">
.en-markup-crop-options {
    top: 18px !important;
    left: 50% !important;
    margin-left: -100px !important;
    width: 200px !important;
    border: 2px rgba(255,255,255,.38) solid !important;
    border-radius: 4px !important;
}

.en-markup-crop-options div div:first-of-type {
    margin-left: 0px !important;
}
</style><style type="text/css">#basic_info_a:after {background-image: url(../common_img/ico-arrow-up.png);}</style>
</head>
