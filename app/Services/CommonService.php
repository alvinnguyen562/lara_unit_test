<?php

namespace App\Services;

use App\Cemetery;

class CommonService
{
    public function getCemetery()
    {
        $listCemeteries = Cemetery::all();
        return $listCemeteries;
    }

    public function dispHead($class = '')
    {
        $cemetery_name = $this->getCemetery();
        $head =
            '<header role="banner" id="header-main" data-text="' . $cemetery_name[0]['CEMETERY_NAME'] . '">
                <div class="area-header">
                    <h1 class="' . $class . '"><a href="https://www.e-ohaka.com/sp/"><img src="assets/img/header-logo.png" alt="霊園と墓地と墓石店選び「いいお墓」"></a></h1>

                    <p class="img-tel"><a href="tel:0120949938"><img src="assets/img/head-tel.png" alt="フリーダイヤル"></a></p>

                    <a href="https://www.e-ohaka.com/sp/history/" class="img-rireki">最近見た霊園</a>
                    <a id="slide_menu" href="#sidr" class="menu01"><span>MENU</span></a>
                    <div id="main_overlay"><span><img src="assets/img/batsu.png"></span></div>
                    <div id="sidr" style="margin: 0px; height: 100%; transition: right 0.2s ease;" class="sidr right">
                        <nav id="main-menu" style="position: relative;display:none;overflow:auto;height:100%;">
                            <div class="menu-inner" style="width:100%;overflow-y:auto;height:100%;">

                                <p class="banner"><a href="https://www.e-ohaka.com/sp/campaign2/" target="_blank"><img src="assets/img/bnr_tour.png" alt="霊園の現地見学キャンペーン実施中! 3000円分プレゼント！ JCBギフトカード"></a></p>

                                <h2>霊園・墓地を探す</h2>
                                <ul>
                                    <li class="ico-menu-5"><a href="https://www.e-ohaka.com/sp/area/" title="地域から探す">地域から探す</a></li>
                                    <li class="ico-menu-6"><a href="https://www.e-ohaka.com/sp/line/" title="路線・駅から探す">路線・駅から探す</a></li>
                                </ul>
                                <h2>お墓をたてる</h2>
                                <ul>
                                    <li class="ico-menu-13"><a href="https://www.e-ohaka.com/boseki_catalog/" title="墓石のカタログを請求する">墓石のカタログを請求する</a></li>
                                </ul>

                                <h2>特色・こだわりから探す</h2>
                                <ul class="list-icon mb-80">
                                    <li class="icon-05"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=f14" title="一般墓">一般墓</a></li>
                                    <li class="icon-06"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=9" title="樹木葬">樹木葬</a></li>
                                    <li class="icon-07"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=2" title="永代供養墓">永代供養墓</a></li>
                                    <li class="icon-08"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=3" title="納骨堂">納骨堂</a></li>
                                    <li class="icon-09"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=13" title="ガーデニング霊園">ガーデニング霊園</a></li>
                                    <li class="icon-10"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=7" title="駅近・アクセス至便">駅近・アクセス至便</a></li>
                                    <li class="icon-11"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=1" title="新規開園">新規開園</a></li>
                                    <li class="icon-12"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=4" title="ペットと一緒に入れる">ペットと一緒に入れる</a></li>
                                    <li class="icon-13"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=56" title="海・富士山が見える">海・富士山が見える</a></li>
                                    <li class="icon-14"><a href="https://www.e-ohaka.com/sp/area_list/category5/?s_cd=2" title="公営霊園">公営霊園</a></li>
                                    <li class="icon-15"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=12" title="高級霊園">高級霊園</a></li>
                                    <li class="icon-16"><a href="https://www.e-ohaka.com/sp/area_list/category5/?s_cd=3" title="寺院墓地">寺院墓地</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>';
        return $head;
    }

    public function dispFootTelBanner()
    {
        $footerBanner =
            '<div class="tel-banner">
                <a href="tel:0120-949-938"><img src="./sp-top_files/img_tel.png" alt="いいお墓 お客様センター" width="584" height="188"></a>
            </div>';
        return $footerBanner;
    }

    public function dispFoot()
    {
        $footer =
            '<!--↓東証ロゴ（170721）-->
            <div class="area-listed">
                <img src="assets/img/logo_listed.png" width="32px" height="36px" alt="東証一部上場" class="logo-listed">
                <p class="txt-listed">「いいお墓」の運営は、1984年創業の出版社である<span>株式会社鎌倉新書（東証一部上場、証券コード：6184）</span>が行っています。</p>
            </div>

            <section class="notice">
                <h5 class="title accordion">サイト利用の注意事項</h5>
                <ul>
                    <li>資料請求・お問い合わせ・各種相談はすべて「無料」です。</li>
                    <li>一度に資料請求できる霊園・墓地数は最大30件となっております。</li>
                    <li>複数回の利用により一人のお客様が30件を超える資料請求をされた場合は、「いいお墓 お客様センター」での相談対応とさせていただきます。</li>
                    <li>資料は霊園または提携石材店よりお届けいたします。複数の霊園・墓地にて資料請求された場合、複数業者からご連絡がいくことがございます。</li>
                    <li>霊園及び提携石材店には、過度な営業は控えていただくよう、常時注意勧告しています。万一、行き過ぎた営業行為等があった場合は、「いいお墓 お客様センター」にて迅速に対処いたしますので、ご一報ください。</li>
                    <li>空き区画の状況は常時変動がございます。また、価格改訂等により、掲載情報が実際とは異なる場合もございます。</li>
                    <li>掲載されている情報に関して、事実と異なる情報、誤解を招く表記などがございましたら、<a href="https://www.e-ohaka.com/contact/">こちら</a>までご連絡をお願いいたします。</li>
                </ul>
            </section>
            <nav role="navigation" id="nav">
                <ul class="featured">
                    <li><a href="https://www.e-ohaka.com/sp/remodel/" style="background: url(&#39;/sp/img/bg_sp.png&#39;) no-repeat right center;"><img src="assets/img/img_02.jpg" alt="img_01" width="62" height="62"><span>お墓をたてる、引越す</span></a></li>
                    <li><a href="https://www.e-ohaka.com/sp/knowledge/" style="background: url(/sp/img/bg_sp.png) no-repeat right center;"><img src="assets/img/img_01.jpg" alt="img_01" width="62" height="62"><span>お墓について知る</span></a></li>
                </ul>
                <ul class="regular clearfix">
                    <li><a href="https://www.e-ohaka.com/research/">消費者調査</a></li>
                    <li><a href="https://www.e-ohaka.com/sp/kakinokizaka/column/" style="background: url(/sp/img/bg_sp.png) no-repeat right center;">お墓コラム</a></li>
                    <li><a href="https://www.e-ohaka.com/sp/knowledge/q_a/" style="background: url(/sp/img/bg_sp.png) no-repeat right center;">よくある質問</a></li>
                    <li><a href="https://www.e-ohaka.com/sp/knowledge/glossary/" style="background: url(/sp/img/bg_sp.png) no-repeat right center;">用語集</a></li>
                    <li><a href="https://www.e-ohaka.com/company/">運営会社</a></li>
                    <li class="mini"><a href="https://www.e-ohaka.com/media/">メディア掲載情報</a></li>
                    <li class="mini"><a href="https://www.e-ohaka.com/policy/site_policy.html">サイトポリシー</a></li>
                    <li><a href="https://www.e-ohaka.com/policy/terms_of_service.html">利用規約</a></li>
                    <li class="mini"><a href="https://www.e-ohaka.com/policy/privacy_policy.html">プライバシーポリシー</a></li>
                    <li><a href="https://www.e-ohaka.com/site_map/">サイトマップ</a></li>
                    <li><a href="https://www.e-ohaka.com/contact/">お問い合わせ</a></li>
                    <li class="mini"><a href="https://www.e-ohaka.com/sp/contact_partner/" style="background: url(/sp/img/bg_sp.png) no-repeat right center;">掲載・提携のお問い合わせ</a></li>
                </ul>
                <div class="to-top">
                    <a href="https://www.e-ohaka.com/sp/#">このページのトップへ</a>
                </div>
                <!--/.nav-inner -->
            </nav>

            <aside class="pc-mobile">
                <div>表示方法：<a href="javascript:void(0);" onclick="javascript:chnge_sitetype_pc();return false;">PC</a>｜<span>モバイル</span></div>
            </aside>
            <footer role="contentinfo" id="footer">
                <address>Copyright (C) Kamakura Shinsho, Ltd. All Rights Reserved.</address>
            </footer>';
        return $footer;
    }

    public function DispHeadNew()
    {
        $var =
            '<header role="banner" id="header-main-02">
                <div class="area-header">
                    <h1 class="logo"><a href="https://www.e-ohaka.com/sp/"><img src="./sp-detail-review-list_files/header-logo.png" alt="霊園と墓地と墓石店選び「いいお墓」"></a></h1>

                            <p class="img-tel"><a href="tel:0120949938"><img src="./sp-detail-review-list_files/head-tel.png" alt="フリーダイヤル"></a></p>

                    <a href="https://www.e-ohaka.com/sp/history/" class="img-rireki">最近見た霊園</a>
                    <a id="slide_menu" href="https://www.e-ohaka.com/sp/detail_review_list/id1177059049-925450.html#sidr" class="menu01"><span>MENU</span></a>

                        <div id="main_overlay"><span><img src="./sp-detail-review-list_files/batsu.png"></span></div>
                        <div id="sidr" class="sidr right" style="transition: right 0.2s ease;">
                        <nav id="main-menu" style="position: relative;display:none;">
                            <div class="menu-inner" style="width:100%;">
                    <ul class="list-icon"><li class="icon-01"><a href="https://www.e-ohaka.com/sp/history/" title="最近見た霊園">最近見た霊園（1件）</a></li></ul>
                                <p class="banner"><a href="https://www.e-ohaka.com/sp/campaign2/?cid=1177059049-925450&amp;btn_type=2" target="_blank"><img src="./sp-detail-review-list_files/bnr_tour.png" alt="霊園の現地見学キャンペーン実施中! 3000円分プレゼント！ JCBギフトカード"></a></p>

                                <h2>霊園・墓地を探す</h2>
                                <ul>
                                    <li class="ico-menu-5"><a href="https://www.e-ohaka.com/sp/area/" title="地域から探す">地域から探す</a></li>
                                    <li class="ico-menu-6"><a href="https://www.e-ohaka.com/sp/line/" title="路線・駅から探す">路線・駅から探す</a></li>
                                </ul>
                                <h2>お墓をたてる</h2>
                                <ul>
                                    <li class="ico-menu-13"><a href="https://www.e-ohaka.com/boseki_catalog/" title="墓石のカタログを請求する">墓石のカタログを請求する</a></li>
                                </ul>
                            <h2>特集から探す</h2><ul class="list-toku"><li><a href="https://www.e-ohaka.com/sp/osusume_tokyo/"><img src="./sp-detail-review-list_files/1518505549-878101.jpg" class="icon-img" alt=""><p>スタッフがおススメする<br>霊園10選</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/gardening.html"><img src="./sp-detail-review-list_files/1518505589-828707.jpg" class="icon-img" alt=""><p>花咲く人気の<br>ガーデニング霊園特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/noukotsudou.html"><img src="./sp-detail-review-list_files/1518505655-832083.jpg" class="icon-img" alt=""><p>天候に左右されない<br>屋内納骨堂特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/jyumokusou.html"><img src="./sp-detail-review-list_files/1518505684-166349.jpg" class="icon-img" alt=""><p>自然に還る<br>樹木葬特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/special/eitaikuyou.html"><img src="./sp-detail-review-list_files/1518505712-525432.jpg" class="icon-img" alt=""><p>永続的に管理・供養してもらえる<br>永代供養墓特集</p></a></li><li><a href="https://www.e-ohaka.com/sp/detail/id1506484231-143576.html"><img src="./sp-detail-review-list_files/1518505993-048473.jpg" class="icon-img" alt=""><p>町屋駅より徒歩3分。先進納骨堂38万円～<br>東京御廟　本館</p></a></li></ul>
                                <h2>特色・こだわりから探す</h2>
                                <ul class="list-icon mb-80">
                                    <li class="icon-05"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=f14" title="一般墓">一般墓</a></li>
                                    <li class="icon-06"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=9" title="樹木葬">樹木葬</a></li>
                                    <li class="icon-07"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=2" title="永代供養墓">永代供養墓</a></li>
                                    <li class="icon-08"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=3" title="納骨堂">納骨堂</a></li>
                                    <li class="icon-09"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=13" title="ガーデニング霊園">ガーデニング霊園</a></li>
                                    <li class="icon-10"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=7" title="駅近・アクセス至便">駅近・アクセス至便</a></li>
                                    <li class="icon-11"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=1" title="新規開園">新規開園</a></li>
                                    <li class="icon-12"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=4" title="ペットと一緒に入れる">ペットと一緒に入れる</a></li>
                                    <li class="icon-13"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=56" title="海・富士山が見える">海・富士山が見える</a></li>
                                    <li class="icon-14"><a href="https://www.e-ohaka.com/sp/area_list/category5/?s_cd=2" title="公営霊園">公営霊園</a></li>
                                    <li class="icon-15"><a href="https://www.e-ohaka.com/sp/area_list/category5/?f_cd=12" title="高級霊園">高級霊園</a></li>
                                    <li class="icon-16"><a href="https://www.e-ohaka.com/sp/area_list/category5/?s_cd=3" title="寺院墓地">寺院墓地</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>

                </div>

            </header>';
        return $var;
    }
}
