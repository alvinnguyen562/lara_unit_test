<?php

namespace App\Service;

use App\Service\FooTestService;

class BarTestService
{
    /**
     * The BarService instance.
     *
     * @var BarService
     */
    protected $foo;

    /**
     * Create new instance of FooService.
     *
     */
    public function __construct(FooTestService $foo)
    {
        $this->foo = $foo;
    }

    /**
     * Do something useful.
     *
     * @return string
     */
    public function doSomethingBar()
    {
        return $this->foo->somthingToDoFoo();
    }

    public function printText()
    {
        return "tuan";
    }
}
