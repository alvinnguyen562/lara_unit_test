<?php

use Faker\Generator as Faker;

$factory->define(App\Cemetery::class, function (Faker $faker) {
    return [
        'CEMETERY_NAME' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
