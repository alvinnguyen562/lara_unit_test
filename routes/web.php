<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// App::bind('TuanService', function () {
//  return new App\Service\FooTestService();
// });
// $var1 = App::make('TuanService');

// $var2 = $var1->somthingToDo();

// App::bind('Foo', function () {
//  return new App\Service\BarTestService(new App\Service\FooTestService());
// });

// App::bind('Foo', new App\Service\BarTestService(new App\Service\FooTestService()));

// App::bind('Foo', 'App\Service\BarTestService');

// $var3 = App::make('Foo');

// $var4 = $var3->doSomethingBar();

// dd($var4);
// Route::get('/role',[
//     'middleware' => 'role:superadmin',
//     'uses' => 'MainController@checkRole',
// ]);

Route::get('/', function () {
    return view('welcome');
});

Route::resource('posts', 'PostController');
Route::resource('avatar', 'MainController');

// Task
Route::resource('task', 'TaskController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// SP top
Route::get('/sp', 'Sp\TopController@index');

// SP detail
Route::get('/sp/detail', 'Sp\TopController@detail');
Route::get('/sp/detail_review_list', 'Sp\TopController@detail_review_list');
